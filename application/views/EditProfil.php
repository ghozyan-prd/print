<html lang="en">
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="bs-example">
    <h1 style='text-align: center;'>Edit Profil </h1>
    <form  class="form-horizontal" <?php echo form_open_multipart('EditProfil/edit');?>
       <input type="hidden" name="level" value="pengguna">
        <div class="form-group">
            <label class="control-label col-xs-2" for="inputEmail">Email:</label>
            <div class="col-xs-8">
                <input type="email" name="email" class="form-control" id="inputEmail" value="<?php echo $email; ?>" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="inputPassword">Kata Sandi:</label>
            <div class="col-xs-8">
                <input type="password" name="password" class="form-control" value="<?php echo $password; ?>" id="inputPassword">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="Nama">Nama Awal:</label>
            <div class="col-xs-8">
                <input type="text" name="nama_awal" class="form-control" value="<?php echo $nama_awal; ?>" id="Namaawal">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="Nama">Nama Akhir:</label>
            <div class="col-xs-8">
                <input type="text" name="nama_akhir" class="form-control" id="Namaakhir" value="<?php echo $nama_akhir; ?>">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="telp">No. Telp:</label>
            <div class="col-xs-8">
                <input type="tel" name="telp" class="form-control" id="telp" value="<?php echo $telp; ?>">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="Alamat">Alamat:</label>
            <div class="col-xs-8">
                <textarea rows="8" name="alamat" class="form-control" id="Alamat"><?php echo $alamat; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="KodePos">Kode Pos:</label>
            <div class="col-xs-8">
                <input type="text" name="pos" class="form-control" value="<?php echo $pos; ?>" id="KodePos">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2">Jenis Kelamin:</label>
            <div class="col-xs-8">
                <input type="text" name="jk" class="form-control" value="<?php echo $jk; ?>" id="jk" readonly>
            </div>
        </div>
         <div class="form-group">
            <label class="control-label col-xs-2" for="foto">Photo Profil:</label>
            <div class="col-xs-8">
                <input type="file" name="userfile"><?php echo $foto; ?>
            </div>
        <br><br><br>
        <div class="form-group">
            <div class="col-xs-offset-3 col-xs-15">
                <input type="submit" class="btn btn-primary" value="Edit" onClick="return confirm('Apakah Anda Yakin?')">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
        </div>
    </form>
</div>
</body>
</html>                            