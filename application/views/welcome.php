<!DOCTYPE html>
<!--[if lt IE 9 ]><html class="no-js oldie" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js oldie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>

    <!--- basic page needs
    ================================================== -->
    <meta charset="utf-8">
    <title>OPrint</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- mobile specific metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/home/css/base.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/home/css/vendor.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/home/css/main.css">

    <!-- script
    ================================================== -->
    <script src="<?php echo base_url(); ?>aset/home/js/modernizr.js"></script>
    <script src="<?php echo base_url(); ?>aset/home/js/pace.min.js"></script>

    <!-- favicons
    ================================================== -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>aset/home/logo.png" type="image/png">        
</head>

<body id="top">

    <!-- header
    ================================================== -->
    <header class="s-header">

        <div class="header-logo">
            <a class="site-logo" href="index.html">
                <img src="<?php echo base_url(); ?>aset/home/images/logo_transparan.png" alt="Homepage">
            </a>
        </div>

        <nav class="header-nav">

            <a href="#0" class="header-nav__close" title="close"><span>Close</span></a>

            <div class="header-nav__content">
                <h3>Navigation</h3>
                
                <ul class="header-nav__list">
                    <li class="current"><a class="smoothscroll"  href="#home" title="home">Home</a></li>
                    <li><a class="smoothscroll"  href="#about" title="about">About Us</a></li>
                    <li><a class="smoothscroll"  href="#services" title="services">Pelayanan Kami</a></li>
                    <li><a class="smoothscroll"  href="#clients" title="works">Pengguna Kami</a></li>
                    <li><a class="smoothscroll"  href="#team" title="clients">Our Team</a></li>
                </ul>
    
                <p>Kami melayani Jasa Percetakan Dokumen dan File secara Online sehingga membantu masyarakat yang sedang membutuhkan</p>

            </div> <!-- end header-nav__content -->

        </nav>  <!-- end header-nav -->

        <a class="header-menu-toggle" href="#0">
            <span class="header-menu-text">Menu</span>
            <span class="header-menu-icon"></span>
        </a>

    </header> <!-- end s-header -->


    <!-- home
    ================================================== -->
    <section id="home" class="s-home target-section" data-parallax="scroll" data-image-src="<?php echo base_url(); ?>aset/home/images/bg.jpg" data-natural-width=3000 data-natural-height=2000 data-position-y=center>

        <div class="overlay"></div>
        <div class="shadow-overlay"></div>

        <div class="home-content">

            <div class="row home-content__main">

                <h3>Welcome to OPrint</h3>

                <h1>
                    Kami Adalah Pemberi Layanan Jasa
                    Yang Membantu Masyarakat Dalam
                    Hal Percetakan Dokumen Dan File.
                </h1>

                <div class="home-content__buttons">
                    <a href="<?php echo base_url(); ?>index.php/login" class="btn btn--stroke">
                        LOGIN
                    </a>
                    <a href="<?php echo base_url(); ?>index.php/daftar" class="btn btn--stroke">
                        DAFTAR SEKARANG
                    </a>
                </div>

            </div>

            <div class="home-content__scroll">
                <a href="#about" class="scroll-link smoothscroll">
                    <span>Scroll Ke Bawah</span>
                </a>
            </div>

            <div class="home-content__line"></div>

        </div> <!-- end home-content -->

    </section> <!-- end s-home -->


    <!-- about
    ================================================== -->
    <section id='about' class="s-about">

        <div class="row section-header has-bottom-sep" data-aos="fade-up">
            <div class="col-full">
                <h3 class="subhead subhead--dark">Assalamualaikum Wr. Wb.</h3>
                <h1 class="display-1 display-1--light">We Are OPrint</h1>
            </div>
        </div> <!-- end section-header -->

        <div class="row about-desc" data-aos="fade-up">
            <div class="col-full">
                <p>
                    Kami adalah Perusahaan yang melayani Jasa Percetakan Dokumen dan File secara Online sehingga membantu masyarakat yang sedang membutuhkannya dengan cepat.
                </p>
            </div>
        </div> <!-- end about-desc -->

        <div class="row about-stats stats block-1-4 block-m-1-2 block-mob-full" data-aos="fade-up">
                
            <div class="col-block stats__col ">
                <div class="stats__count">1575</div>
                <h5>Pengguna Aktif</h5>
            </div>
            <div class="col-block stats__col">
                <div class="stats__count">10521</div>
                <h5>Dokumen/File Tercetak</h5>
            </div>
            <div class="col-block stats__col">
                <div class="stats__count">100</div>
                <h5>Pegawai</h5>
            </div>
            <div class="col-block stats__col">
                <div class="stats__count">95</div>
                <h5>Penghargaan</h5> 
            </div>

        </div> <!-- end about-stats -->

        <div class="about__line"></div>

    </section> <!-- end s-about -->


    <!-- services
    ================================================== -->
    <section id='services' class="s-services">

        <div class="row section-header has-bottom-sep" data-aos="fade-up">
            <div class="col-full">
                <h3 class="subhead">Apa Saja yang Kami Kerjakan?</h3>
                <h1 class="display-2">Kami melakukan Pencetakan File atau Dokumen sesuai dengan Permintaan Pengguna</h1>
            </div>
        </div> <!-- end section-header -->

        <div class="row services-list block-1-2 block-tab-full">

            <div class="col-block service-item" data-aos="fade-up">
                <div class="service-icon">
                    <img src="<?php echo base_url(); ?>aset/home/images/icon/ribbon.png">
                </div>
                <div class="service-text">
                    <h3 class="h2">Banner</h3>
                    <!--<p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium. 
                    Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.
                    Sunt suscipit voluptas ipsa in tempora esse soluta sint.
                    </p>-->
                </div>
            </div>

            <div class="col-block service-item" data-aos="fade-up">
                <div class="service-icon">
                    <img src="<?php echo base_url(); ?>aset/home/images/icon/docs.png">
                </div>
                <div class="service-text">
                    <h3 class="h2">Document</h3>
                    <!--<p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium. 
                    Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.
                    Sunt suscipit voluptas ipsa in tempora esse soluta sint.
                    </p>-->
                </div>
            </div>

            <div class="col-block service-item" data-aos="fade-up">
                <div class="service-icon">
                    <img src="<?php echo base_url(); ?>aset/home/images/icon/poster.png">
                </div>  
                <div class="service-text">
                    <h3 class="h2">Poster</h3>
                    <!--<p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium. 
                    Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.
                    Sunt suscipit voluptas ipsa in tempora esse soluta sint.
                    </p>-->
                </div>
            </div>

            <div class="col-block service-item" data-aos="fade-up">
                <div class="service-icon">
                    <img src="<?php echo base_url(); ?>aset/home/images/icon/photo.png">
                </div>
                <div class="service-text">
                    <h3 class="h2">Foto</h3>
                    <!--<p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium. 
                    Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.
                    Sunt suscipit voluptas ipsa in tempora esse soluta sint.
                    </p>-->
                </div>
            </div>

            <div class="col-block service-item" data-aos="fade-up">
                <div class="service-icon">
                    <img src="<?php echo base_url(); ?>aset/home/images/icon/id-card.png">
                </div>
                <div class="service-text">
                    <h3 class="h2">Kartu Nama</h3>
                    <!--<p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium. 
                    Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.
                    Sunt suscipit voluptas ipsa in tempora esse soluta sint.
                    </p>-->
                </div>
            </div>
    
            <div class="col-block service-item" data-aos="fade-up">
                <div class="service-icon">
                    <img src="<?php echo base_url(); ?>aset/home/images/icon/certificate.png">
                </div>
                <div class="service-text">
                    <h3 class="h2">Sertifikat</h3>
                    <!--<p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium. 
                    Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.
                    Sunt suscipit voluptas ipsa in tempora esse soluta sint.
                    </p>-->
                </div>
            </div>

            <div class="col-block service-item" data-aos="fade-up">
                <div class="service-icon">
                    <img src="<?php echo base_url(); ?>aset/home/images/icon/sticker.png">
                </div>
                <div class="service-text">
                    <h3 class="h2">Stiker</h3>
                    <!--<p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium. 
                    Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.
                    Sunt suscipit voluptas ipsa in tempora esse soluta sint.
                    </p>-->
                </div>
            </div>

            <div class="col-block service-item" data-aos="fade-up">
                    <div class="service-icon">
                        <img src="<?php echo base_url(); ?>aset/home/images/icon/flyer.png">
                    </div>
                    <div class="service-text">
                        <h3 class="h2">Pamflet</h3>
                        <!--<p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium. 
                        Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.
                        Sunt suscipit voluptas ipsa in tempora esse soluta sint.
                        </p>-->
                    </div>
                </div>   

        </div> <!-- end services-list -->

    </section> <!-- end s-services -->


    <!-- clients
    ================================================== -->
    <section id="clients" class="s-clients">
        <div class="row section-header" data-aos="fade-up">
            <div class="col-full">
                <h3 class="subhead">Pengguna Layanan Kami</h3>
                <h1 class="display-2">Perusahaan OPrint Sudah Melayani Banyak Pengguna Dari
                    Berbagai Kalangan
                </h1>
            </div>
        </div> <!-- end section-header -->

        <div class="row clients-outer" data-aos="fade-up">
            <div class="col-full">
                <div class="clients">
                    
                    <a href="#0" title="" class="clients__slide"><img src="<?php echo base_url(); ?>aset/home/images/icon/schstudent.png" /></a>
                    <a href="#0" title="" class="clients__slide"><img src="<?php echo base_url(); ?>aset/home/images/icon/schstudent2.png" /></a>
                    <a href="#0" title="" class="clients__slide"><img src="<?php echo base_url(); ?>aset/home/images/icon/graduated.png" /></a>
                    <a href="#0" title="" class="clients__slide"><img src="<?php echo base_url(); ?>aset/home/images/icon/graduated2.png"  /></a>
                    <a href="#0" title="" class="clients__slide"><img src="<?php echo base_url(); ?>aset/home/images/icon/employee.png" /></a>
                    <a href="#0" title="" class="clients__slide"><img src="<?php echo base_url(); ?>aset/home/images/icon/employee (1).png" /></a>
                    <!--<a href="#0" title="" class="clients__slide"><img src="<?php echo base_url(); ?>aset/home/images/clients/joomla.png" /></a>-->
                    <!--<a href="#0" title="" class="clients__slide"><img src="<?php echo base_url(); ?>aset/home/images/clients/magento.png" /></a>-->
                     
                </div> <!-- end clients -->
            </div> <!-- end col-full -->
        </div> <!-- end clients-outer -->
    </section>

        <section id="team" class="s-team" style="background-color: cornsilk;">
        <div class="row section-header" data-aos="fade-up">
                <div class="col-full">
                <br>

                </br>
                    <h1 style="color: grey" class="display-2">OUR TEAM
                    </h1>
                </div>
            </div> 
        <div class="row clients-testimonials" data-aos="fade-up">
            <div class="col-full">
                <div class="testimonials">

                    <div class="testimonials__slide">

                        <p>Do The Best, Get The Best</p>

                        <img src="<?php echo base_url(); ?>aset/home/images/avatars/sur.png" alt="Author image" class="testimonials__avatar">
                        <div class="testimonials__info">
                            <span style="color: grey" class="testimonials__name">
                                Sururin Darina<br>
                                H06216025
                            </span> 
                            <span style="color: grey" class="testimonials__pos">Manager</span>
                        </div>
                    </div>

                    <div class="testimonials__slide">

                            <p>Jangan Bersedih Jika Kamu Tidak Dihargai, Tapi Bersedihlah Jika Kamu Tidak Berharga Lagi.</p>
    
                            <img src="<?php echo base_url(); ?>aset/home/images/avatars/dhea.png" alt="Author image" class="testimonials__avatar">
                            <div class="testimonials__info">
                                <span style="color: grey" class="testimonials__name">
                                    Dhea Silvi<br>
                                    H76216031
                                </span> 
                                <span style="color: grey" class="testimonials__pos">Analysis Team</span>
                            </div>
                    </div>

                    <div class="testimonials__slide">

                            <p>Tetap Tersenyumlah Meskipun Menghadapi Banyak Rintangan Yang Menghantuimu</p>
    
                            <img src="<?php echo base_url(); ?>aset/home/images/avatars/elita.png" alt="Author image" class="testimonials__avatar">
                            <div class="testimonials__info">
                                <span style="color: grey" class="testimonials__name">
                                    Elita Mega Tiana<br>
                                    H76216033
                                </span> 
                                <span style="color: grey" class="testimonials__pos">Analysis Team</span>
                            </div>
                    </div>

                    <div class="testimonials__slide">

                            <p>The Best Way To Predict The Future Is To Create It</p>
    
                            <img src="<?php echo base_url(); ?>aset/home/images/avatars/fajar.png" alt="Author image" class="testimonials__avatar">
                            <div class="testimonials__info">
                                <span style="color: grey" class="testimonials__name">
                                    MH Ainun Fajar<br>
                                    H06216013
                                </span>
                                <span style="color: grey" class="testimonials__pos">Programming Team</span>
                            </div>
                    </div>

                    <div class="testimonials__slide">

                            <p>I Walk Slowly, But I Never Walk Backwalk</p>
    
                            <img src="<?php echo base_url(); ?>aset/home/images/avatars/ghoz2.png" alt="Author image" class="testimonials__avatar">
                            <div class="testimonials__info">
                                <span style="color: grey" class="testimonials__name">
                                    Ghozyan Hilman Pradana<br>
                                    H06216008
                                </span>
                                <span style="color: grey" class="testimonials__pos">Programming Team</span>
                            </div>
                    </div>

                    <div class="testimonials__slide">

                            <p>Wish All Your Dreams Come True</p>
    
                            <img src="<?php echo base_url(); ?>aset/home/images/avatars/riski.jpg" alt="Author image" class="testimonials__avatar">
                            <div class="testimonials__info">
                                <span style="color: grey" class="testimonials__name">
                                    Rizki Ramdhani<br>
                                    H76216074
                                </span>
                                <span style="color: grey" class="testimonials__pos">Programming Team</span>
                            </div>
                    </div>

                    <div class="testimonials__slide">

                            <p>Before You Change the World, You Must Change Yourself First</p>
    
                            <img src="<?php echo base_url(); ?>aset/home/images/avatars/rey.jpeg" alt="Author image" class="testimonials__avatar">
                            <div class="testimonials__info">
                                <span style="color: grey" class="testimonials__name">
                                    Reynaldi Octa Pradana<br>
                                    H76216073
                                </span>
                                <span style="color: grey" class="testimonials__pos">Database Team</span>
                            </div>
                    </div>

                    <div class="testimonials__slide">

                            <p>Jadilah Kamu Seperti Processor, Kecil Di Mata Orang Tapi Besar Perananmu Di Mata Masyarakat</p>
    
                            <img src="<?php echo base_url(); ?>aset/home/images/avatars/randa.png" alt="Author image" class="testimonials__avatar">
                            <div class="testimonials__info">
                                <span style="color: grey" class="testimonials__name">
                                    Randa Wahyu Saputra<br>
                                    H76216072
                                </span>
                                <span style="color: grey" class="testimonials__pos">Database Team</span>
                            </div>
                    </div>

                </div><!-- end testimonials -->
                
            </div> <!-- end col-full -->
        </div> <!-- end client-testimonials -->


    <!-- footer
    ================================================== -->
    <footer>

        <div class="row footer-bottom">

            <div class="col-twelve">
                <div class="copyright" style="color: grey;">
                    <span>© Copyright Kelompok 1 Interkasi Manusia Dan Komputer</span><br>
                    <span>Prodi Sistem Informasi</span>
                    <span>Fakultas Sains dan Teknologi</span>
                    <span>UIN Sunan Ampel Surabaya</span>	
                </div>

                <div class="go-top">
                    <a class="smoothscroll" title="Back to Top" href="#top"><i class="icon-arrow-up" aria-hidden="true"></i></a>
                </div>
            </div>

        </div> <!-- end footer-bottom -->

    </footer> <!-- end footer -->


    <!-- photoswipe background
    ================================================== -->
    <div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">

        <div class="pswp__bg"></div>
        <div class="pswp__scroll-wrap">

            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <div class="pswp__ui pswp__ui--hidden">
                <div class="pswp__top-bar">
                    <div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Close (Esc)"></button> <button class="pswp__button pswp__button--share" title=
                    "Share"></button> <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button> <button class="pswp__button pswp__button--zoom" title=
                    "Zoom in/out"></button>
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button> <button class="pswp__button pswp__button--arrow--right" title=
                "Next (arrow right)"></button>
                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>
            </div>

        </div>

    </div> <!-- end photoSwipe background -->


    <!-- preloader
    ================================================== -->
    <div id="preloader">
        <div id="loader">
            <div class="line-scale-pulse-out">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>


    <!-- Java Script
    ================================================== -->
    <script src="<?php echo base_url(); ?>aset/home/js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/home/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>aset/home/js/main.js"></script>

</body>

</html>