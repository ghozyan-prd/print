<div class="buy-now">
		<div class="container">
		<div class="heading-setion-w3ls">
			<h3 class="title-w3layouts">Pilih Jenis Cetak</h3>
		</div>
				<div class="team-grids">
					<!-- Bottom to top-->
					<div class="details-top-w3ls">
						<div class="col-md-3 team-grid">
							<!-- normal -->
							<div class="ih-item circle effect10 bottom_to_top">
								<div class="img"><img  src="<?php echo base_url(); ?>aset/images/print/banner.jpg" alt="img"> <p style="color: #000;background-color: white;font-size: 17px">X - Banner</p> <span>#002</span></div>
								<div class="info">
									<h3><span></span></h3>
									<ul>
										
										<li class="cary-li-w3-agileits">
											<div class="snipcart-details top_brand_home_details">
												<form action="kalkulator" method="Post">
                                                 <input type="hidden" name="jenis" value="X-Banner" >
                                                  <input type="hidden" name="harga" value="50000" >
                                                  <input type="hidden" name="jenisukuran" value="X_Banner" >
													<input type="submit" name="submit" value="Pesan" class="button" />
												</form>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<!-- end normal -->
						</div>
						<div class="col-md-3 team-grid">
							<!-- normal -->
							<div class="ih-item circle effect10 bottom_to_top">
								<div class="img"><img  src="<?php echo base_url(); ?>aset/images/print/dokumen.jpg" alt="img"> <p style="color: #000;background-color: white;font-size: 17px">Document</p> <span>#002</span></div>
								<div class="info">
									<h3><span></span></h3>
									<ul>
										
										<li class="cary-li-w3-agileits">
											<div class="snipcart-details top_brand_home_details">
												<form action="kalkulator" method="Post">
                                                 <input type="hidden" name="jenis" value="Document" >
                                                  <input type="hidden" name="harga" value="50000" >
                                                  <input type="hidden" name="jenisukuran" value="Dokumen" >
													<input type="submit" name="submit" value="Pesan" class="button" />
												</form>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<!-- end normal -->
						</div>
						<div class="col-md-3 team-grid">
							<!-- normal -->
							<div class="ih-item circle effect10 bottom_to_top">
								<div class="img"><img  src="<?php echo base_url(); ?>aset/images/print/poster.jpg" alt="img"> <p style="color: #000;background-color: white;font-size: 17px">Poster</p> <span>#003</span></div>
								<div class="info">
									<h3><span></span></h3>
									<ul>
										
										<li class="cary-li-w3-agileits">
											<div class="snipcart-details top_brand_home_details">
												<form action="kalkulator" method="Post">
                                                 <input type="hidden" name="jenis" value="Poster" >
                                                  <input type="hidden" name="harga" value="50000" >
                                                  <input type="hidden" name="jenisukuran" value="Poster" >
													<input type="submit" name="submit" value="Pesan" class="button" />
												</form>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<!-- end normal -->
						</div>
						<div class="col-md-3 team-grid">
							<!-- normal -->
							<div class="ih-item circle effect10 bottom_to_top">
								<div class="img"><img  src="<?php echo base_url(); ?>aset/images/print/foto.jpg" alt="img"> <p style="color: #000;background-color: white;font-size: 17px">Cetak Foto</p> <span>#004</span></div>
								<div class="info">
									<h3><span></span></h3>
									<ul>
										
										<li class="cary-li-w3-agileits">
											<div class="snipcart-details top_brand_home_details">
												<form action="kalkulator" method="Post">
                                                 <input type="hidden" name="jenis" value="Foto" >
                                                  <input type="hidden" name="harga" value="50000" >
                                                  <input type="hidden" name="jenisukuran" value="Foto" >
													<input type="submit" name="submit" value="Pesan" class="button" />
												</form>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<!-- end normal -->
						</div>
					</div>
					<div class="details-mid-w3ls">
						<div class="col-md-3 team-grid">
							<!-- normal -->
							<div class="ih-item circle effect10 bottom_to_top">
								<div class="img"><img src="<?php echo base_url(); ?>aset/images/print/kartunama.jpg" alt="img"><p style="color: #000;background-color: white;font-size: 15px">Kartu Nama</p><span>#005</span></div>
								<div class="info">
									<h3><span></span></h3>
									<ul>
										
										<li class="cary-li-w3-agileits">
											<div class="snipcart-details top_brand_home_details">
												<form action="kalkulator" method="Post">
                                                 <input type="hidden" name="jenis" value="Kartu Nama" >
                                                  <input type="hidden" name="harga" value="50000" >
                                                  <input type="hidden" name="jenisukuran" value="Kartu_nama" >
													<input type="submit" name="submit" value="Pesan" class="button" />
												</form>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<!-- end normal -->
						</div>
						<div class="col-md-3 team-grid">
							<!-- normal -->
							<div class="ih-item circle effect10 bottom_to_top">
								<div class="img"><img  src="<?php echo base_url(); ?>aset/images/print/sertifikat.jpg" alt="img"> <p style="color: #000;background-color: white;font-size: 17px">Sertifikat</p> <span>#006</span></div>
								<div class="info">
									<h3><span></span></h3>
									<ul>
										
										<li class="cary-li-w3-agileits">
											<div class="snipcart-details top_brand_home_details">
												<form action="kalkulator" method="Post">
                                                 <input type="hidden" name="jenis" value="Sertifikat" >
                                                  <input type="hidden" name="harga" value="50000" >
                                                  <input type="hidden" name="jenisukuran" value="Sertifikat" >
													<input type="submit" name="submit" value="Pesan" class="button" />
												</form>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<!-- end normal -->
						</div>
						<div class="col-md-3 team-grid">
							<!-- normal -->
							<div class="ih-item circle effect10 bottom_to_top">
								<div class="img"><img  src="<?php echo base_url(); ?>aset/images/print/stiker.jpg" alt="img"> <p style="color: #000;background-color: white;font-size: 17px">Stiker</p> <span>#007</span></div>
								<div class="info">
									<h3><span></span></h3>
									<ul>
										
										<li class="cary-li-w3-agileits">
											<div class="snipcart-details top_brand_home_details">
												<form action="kalkulator" method="Post">
                                                 <input type="hidden" name="jenis" value="Stiker" >
                                                  <input type="hidden" name="harga" value="50000" >
                                                  <input type="hidden" name="jenisukuran" value="Stiker" >
													<input type="submit" name="submit" value="Pesan" class="button" />
												</form>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<!-- end normal -->
						</div>
						<div class="col-md-3 team-grid">
							<!-- normal -->
							<div class="ih-item circle effect10 bottom_to_top">
								<div class="img"><img  src="<?php echo base_url(); ?>aset/images/print/pamflet.png" alt="img"> <p style="color: #000;background-color: white;font-size: 17px">Pamflet</p> <span>#008</span></div>
								<div class="info">
									<h3><span></span></h3>
									<ul>
										
										<li class="cary-li-w3-agileits">
											<div class="snipcart-details top_brand_home_details">
												<form action="kalkulator" method="Post">
                                                 <input type="hidden" name="jenis" value="Pamflet" >
                                                  <input type="hidden" name="harga" value="50000" >
                                                  <input type="hidden" name="jenisukuran" value="Pamflet" >
													<input type="submit" name="submit" value="Pesan" class="button" />
												</form>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<!-- end normal -->
							<!-- end normal -->
						</div>
					</div>
					<!-- end Bottom to top-->
				</div>
			</div>
	</div>
	<br>
</br>
<br>
</br>
<br>
</br>
<br>
</br>
<br>
</br>
<br>
</br>