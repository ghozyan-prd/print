<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="zxx">
<head>
<title>Online Printing</title>
<link rel="icon" href="<?php echo base_url('aset/home/logo.png'); ?>">
<!-- custom-theme -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="prezzie Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //custom-theme -->
<link href="<?php echo base_url(); ?>aset/css/style1.css" rel="stylesheet" type="text/css" media="all" />

<link href="<?php echo base_url() ?>/aset/css/styles.css" rel='stylesheet' type='text/css'/>
<link href="<?php echo base_url(); ?>aset/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- font-awesome-icons -->

<link href="<?php echo base_url(); ?>aset/css/font-awesome.css" rel="stylesheet"> 
<link href="<?php echo base_url(); ?>aset/css/kalkulator.css" rel="stylesheet" type="text/css" media="all" />
<!-- //font-awesome-icons -->

<link href="<?php echo base_url(); ?>aset/css/owl.carousel.css" rel="stylesheet">

<link href="<?php echo base_url(); ?>aset/css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="//fonts.googleapis.com/css?family=Tangerine:400,700" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
</head>
<body>
<!-- banner -->
<div class="banner_top" id="home">
	<div data-vide-bg ="<?php echo base_url(); ?>aset/video/gift-packs">
		<div class="center-container inner-container">
			<div class="w3_agile_header">
						<div class="w3_agileits_logo">
								<h1><a href="index.html">Printing Online<span>Life is a gift</span></a></h1>
							</div>
							<div class="w3_menu">
							<div class="agileits_w3layouts_banner_info">
				
								 
							</div>
								<div class="top-nav">
								<nav class="navbar navbar-default">
									<div class="container">
										<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">Menu						
										</button>
									</div>
									<!-- Collect the nav links, forms, and other content for toggling -->
									<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
										<ul class="nav navbar-nav">
											<li class="home-icon"><a href=""><span class="fa fa-home" aria-hidden="true"></span></a></li>
											
											<li><a href="gallery">Beranda</a></li>
											<li><a href="history">History</a></li>

											<li><a href="Profil">Profil</a></li>
											<li><a href="about" >About</a></li>
											<li><a href="<?php echo base_url('index.php/logout')?>">Logout</a></li>
											
										</ul>	
										<div class="clearfix"> </div>
									</div>	
								</nav>	
							</div>
						</div>

					<div class="clearfix"></div>
			    </div>
				<!-- banner-text -->
			<h2 class="inner-heading-agileits-w3layouts">Selamat datang, <?php echo $nama_awal; ?></h2>
			<!--banner Slider starts Here-->
      </div>
   </div>
     </div>
<!-- //banner -->
<!-- about -->
	<?php echo $contents;?>
<!-- gallery -->
<!-- //gallery -->

<!-- testimonials -->
<!-- //testimonials -->
<!--footer-->
	<div class="footer_bottom section">
    <div class="agileits-w3layouts-footer">
      <div class="container">
        <div class="col-md-4 w3-agile-grid">
          <h5>About Us</h5>
          <p>Aplikasi Ini dibuat Oleh : <br> Kelompok 1 Mata kuliah Interaksi Manusia dan Komputer. <br> Sistem Informasi 2016 <br> Universitas Islam Negeri Sunan Ampel Surabaya</p>
             <div class="w3_agileits_social_media team_agile_w3l team footer">
                <ul class="social-icons3">
                  
                  <li><a href="#" class="wthree_facebook"> <i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="wthree_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="wthree_dribbble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="wthree_behance"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
                </ul>
              </div>  
        <div class="image-agileits">
        <img src="images/si.jpg" alt="" class="img-r">
        </div>
        <div class="clearfix"> </div>
        </div>
        <div class="col-md-4 w3-agile-grid mid-w3-add">
          <h5>Address</h5>
          <div class="w3-address">
            <div class="w3-address-grid">
              <div class="w3-address-left">
                <i class="fa fa-phone" aria-hidden="true"></i>
              </div>
              <div class="w3-address-right">
                <h6>Phone Number</h6>
                <p>Kelompok 1 IMK</p>
                <p>+6281 273 338 544</p>
              </div>
              <div class="clearfix"> </div>
            </div>
            <div class="w3-address-grid">
              <div class="w3-address-left">
                <i class="fa fa-envelope" aria-hidden="true"></i>
              </div>
              <div class="w3-address-right">
                <h6>Email Address</h6>
                <p>Email :<a href="mailto:example@email.com"> ghozyan20.ozy@gmail.com</a></p>
              </div>
              <div class="clearfix"> </div>
            </div>
            <div class="w3-address-grid">
              <div class="w3-address-left">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
              </div>
              <div class="w3-address-right">
                <h6>Location</h6>
                <p>Jalan Ahmad Yani No. 117 
                <span>Wonocolo, Kota SBY, Jawa Timur 60237</span>
                </p>
              </div>
              <div class="clearfix"> </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 footer-right w3-agile-grid">
          <div class="agile_footer_grid">
          <h5>Latest News</h5>
            <ul class="agileits_w3layouts_footer_grid_list">
              <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                <a href="#" data-toggle="modal" data-target="#myModal1">Discount Bulan Juli </a>
              </li>
              <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                <a href="#" data-toggle="modal" data-target="#myModal1">Discount Khusus Mahasiswa</a>
              </li>
              <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                <a href="#" data-toggle="modal" data-target="#myModal1">Discount Gratis Ongkir </a>
              </li>
              <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                <a href="#" data-toggle="modal" data-target="#myModal1">Discount Hari Kemerdekaan (Agustus)</a>
              </li>
              <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                <a href="#" data-toggle="modal" data-target="#myModal1"> Discount Akhir Tahun</a>
              </li>
            </ul>
          </div>
          <h5>Stay in Touch</h5>
          <form action="#" method="post">
            <input type="email" name="Email" placeholder="Email Id" required="">
            <input type="submit" value="Subscribe">
          </form>
        </div>
        <div class="clearfix"> </div>
      </div>
    </div>
    <div class="copyright">
             <p>© 2018 Information System UINSA. All rights reserved | Design by <a href="http://w3layouts.com">Kelompok 1</a></p>
        </div>
  </div>
<!-- Modal1 -->
            <div class="modal fade" id="myModal1" tabindex="-1" role="dialog">
              <div class="modal-dialog">
              <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4>Prezzie</h4>
                    <img src="images/f2.jpg" alt=" " class="img-responsive">
                    <h5>Integer lorem ipsum dolor sit amet </h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, rds which don't look even slightly believable..</p>
                  </div>
                </div>
              </div>
            </div>
						<!-- //Modal1 -->

<!--//footer-->

	<!-- js -->
<script type="text/javascript" src="<?php echo base_url(); ?>aset/js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<!-- for-Clients -->
		<script src="<?php echo base_url(); ?>aset/js/owl.carousel.js"></script>
			<!-- requried-jsfiles-for owl -->
							        <script>
									    $(document).ready(function() {
									      $("#owl-demo2").owlCarousel({
									        items : 1,
									        lazyLoad : false,
									        autoPlay : true,
									        navigation : false,
									        navigationText :  false,
									        pagination : true,
									      });
									    });
									  </script>
			<!-- //requried-jsfiles-for owl -->
	<!-- //for-Clients -->
 <script type="text/javascript">
					$(window).load(function() {
						$("#flexiselDemo1").flexisel({
							visibleItems: 4,
							animationSpeed: 1000,
							autoPlay: false,
							autoPlaySpeed: 3000,    		
							pauseOnHover: true,
							enableResponsiveBreakpoints: true,
							responsiveBreakpoints: { 
								portrait: { 
									changePoint:568,
									visibleItems: 1
								}, 
								landscape: { 
									changePoint:640,
									visibleItems:2
								},
								tablet: { 
									changePoint:768,
									visibleItems: 3
								}
							}
						});
						
					});
				</script>
				<script type="text/javascript" src="<?php echo base_url(); ?>aset/js/jquery.flexisel.js"></script>
<!-- cart-js -->
	<script src="<?php echo base_url(); ?>aset/js/minicart.min.js"></script>
	<script>
		// Mini Cart
		paypal.minicart.render({
			action: '#'
		});

		if (~window.location.search.indexOf('reset=true')) {
			paypal.minicart.reset();
		}
	</script>
<!-- //cart-js --> 
<!-- video-bg -->
<script src="<?php echo base_url(); ?>aset/js/jquery.vide.min.js"></script>
<!-- //video-bg -->
<!-- Nice scroll -->
<script src="<?php echo base_url(); ?>aset/js/jquery.nicescroll.js"></script>
<script src="<?php echo base_url(); ?>aset/js/scripts.js"></script>
<!-- //Nice scroll -->
<!-- for bootstrap working -->
<script type="text/javascript" src="<?php echo base_url(); ?>aset/js/bootstrap-3.1.1.min.js"></script>
<!-- //for bootstrap working -->
</body>
</html>