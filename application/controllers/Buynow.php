<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buynow extends CI_Controller {
	public function index()
	{
		if ($this->session->userdata('isPengguna') == TRUE) {
            $this->template->load('static','buynow');
        } else {
            redirect(login);
        }
	}
}
