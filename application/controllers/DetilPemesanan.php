<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DetilPemesanan extends CI_Controller {

    public function index() {
    	$username =  $this->session->userdata('username');
        $jumlah = $_POST ['jumlah'];
        $salinan = $_POST ['salinan'];
        $jenisukuran = $_POST ['jenisukuran'];
        $ukuran = $_POST ['ukuran'];
        $warna = $_POST ['warna'];
        $cover = $_POST ['cover'];
          $data = $this->M_register->get_user($username);
            $profil = array(
                "nama_awal" => $data[0]["nama_awal"],
            );
        $isicombo = $this->M_transaksi->getHarga($jenisukuran, $ukuran);
        $harga = 0 + $isicombo[0]["harga"] * $jumlah * $salinan;
        if($warna == "Berwarna"){
            $harga = $harga * 1.4;
        }if($cover == "Cover Berwarna Tebal"){
            $harga = $harga + 3000;
        }
        $harga;
        $user = $this->M_register->get_user($username);
        $data2 = array(
        	"jumlah" => $jumlah,
        	"salinan" => $salinan,
        	"jenisukuran" => $jenisukuran,
        	"ukuran" => $ukuran,
        	"warna" => $warna,
        	"cover" => $cover,
            "harga" => $harga,
            "id" => $user[0]['id'],
            "id_jenis" => $isicombo[0]['id_ukuran'],
            "quantity" =>  $jumlah * $salinan,
            "nama_awal" => $data[0]["nama_awal"],
        );
        	$username = $this->session->userdata('username');
    		$data = $this->M_register->get_user($username);
        	$data = array(
    			"nama_awal" => $data[0]["nama_awal"],
    		);
        $this->template->load('static1', 'DetilPemesanan', $data2);
	}

	public function aksi_upload(){
		$config['upload_path']          = './aset/images/pemesanan/';
		$config['allowed_types']        = 'gif|jpg|png|docx|pdf|doc|xls|xlsx|pptx|ppt';
		$config['max_size']             = 1000000;
		$config['max_width']            = 5000;
		$config['max_height']           = 5000;
 
		$this->load->library('upload', $config);
 
		if ( ! $this->upload->do_upload())
		{
			$this->load->view('V_error');
			
		}
		else{
			$img = $this->upload->data();
			$foto = $img['file_name'];
			$id = $this->input->post('id');
			$id_jenis = $this->input->post('id_jenis');
			$quantity = $this->input->post('quantity');
			$nama_penerima = $this->input->post('nama_penerima');
			$alamat_kirim = $this->input->post('alamat_kirim');
			$harga = $this->input->post('estimasi_harga');
			$data = array(
				'id' => $id,
				'id_jenis' => $id_jenis,
				'quantity' => $quantity,
				'nama_penerima' => $nama_penerima,
				'alamat_kirim' => $alamat_kirim	,
				'estimasi_harga' => $harga,
				'file' => $foto,
								);
			$this->db->insert('transaksi',$data);
			$this->load->view('DetilPemesanan');
			redirect('gallery');
		}
 }
}
