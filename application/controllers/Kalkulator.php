<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kalkulator extends CI_Controller {

    public function index() {
        if ($this->session->userdata('isPengguna') == TRUE) {
            $username = $this->session->userdata('username');
            $data = $this->M_register->get_user($username);
            $profil = array(
                "nama_awal" => $data[0]["nama_awal"],
            );

        $jenis = $_POST ['jenis'];
        $jenisukuran = $_POST ['jenisukuran'];
        $isicombo = $this->M_transaksi->isicombo($jenisukuran );
       
        $this->template->load('static', 'kalkulator', array(
            'jenis' => $jenis,
            'data' => $isicombo,
            'jenisukuran' => $jenisukuran,
             "nama_awal" => $data[0]["nama_awal"],
         ));
        } else {
            redirect(login);
        }
    }

    public function jumlah() {
        $jenisukuran = $_POST ['jenisukuran'];
        $ukuran = $_POST ['ukuran'];
        $isicombo = $this->M_transaksi->isicombo($jenisukuran,$ukuran );
        $harga;
        $data = array(
             "harga" => $isicombo[0]["harga"],
        );
        foreach ($data as $d){
            echo $harga;
        }
    }

}
