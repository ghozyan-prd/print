<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar extends CI_Controller {
	function __construct() {
		parent::__construct();
        $this->load->model('M_register');		
		$this->load->helper(array('url'));

	}
	public function index()
	{
		$this->template->load('static1','daftar');
	}

	public function aksi_upload(){
		$config['upload_path']          = './aset/images/daftarbaru/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 10000;
		$config['max_width']            = 5000;
		$config['max_height']           = 5000;
 
		$this->load->library('upload', $config);
 
		if ( ! $this->upload->do_upload())
		{
			$this->load->view('V_error');
			
		}
		else{
			$img = $this->upload->data();
			$foto = $img['file_name'];
			$nama_awal = $this->input->post('nama_awal');
			$nama_akhir = $this->input->post('nama_akhir');
			$email = $this->input->post('email');
			$alamat = $this->input->post('alamat');
			$pos = $this->input->post('pos');
			$telp = $this->input->post('telp');
			$jk = $this->input->post('jk');
			$password = $this->input->post('password');
			$level = $this->input->post('level');

			$data = array(
				'nama_awal' => $nama_awal,
				'nama_akhir' => $nama_akhir,
				'email' => $email,
				'alamat' => $alamat,
				'pos' => $pos,
				'telp' => $telp,
				'jk' => $jk,
				'foto' => $foto,
				'password' => md5($password),
				'level' => $level,
			);
			$this->db->insert('user',$data);
			$this->load->view('login');
			redirect('login');
		}
	}
}
