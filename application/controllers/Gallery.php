<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

    public function index() {
        if ($this->session->userdata('isPengguna') == TRUE) {
        	$username = $this->session->userdata('username');
    		$data = $this->M_register->get_user($username);
        	$profil = array(
    			"nama_awal" => $data[0]["nama_awal"],
    		);
            $this->template->load('static', 'gallery', $profil);

        } else {
            redirect(login);
        }
    }

}
